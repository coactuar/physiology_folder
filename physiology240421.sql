-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2022 at 04:22 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `physiology240421`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Kalyan Goswami', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'AV ', NULL, NULL, '2021/04/23 15:19:50', '2021-04-23', '2021-04-23', 1, 'Abbott'),
(2, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/04/24 18:25:45', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(3, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/04/24 18:27:12', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(4, 'Muhammed Nizar', 'muhammed.nizar@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021/04/24 18:54:54', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(5, 'Arun James', 'arun.nelluvelil@abbott.com', 'BANGALORE', 'Av', NULL, NULL, '2021/04/24 18:58:34', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(6, 'Dr Sasirekha', 'drsasirekha@gmail.com', 'Mysore', 'Columbia Asia Hospital', NULL, NULL, '2021/04/24 19:00:29', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(7, 'Atanu Banerjee', 'atanu.banerjee@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021/04/24 19:18:13', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(8, 'Dr Vivekanand', 'vivekpgi@yahoo.com', 'DHARWAD', 'SDM Narayana Heart centre', NULL, NULL, '2021/04/24 19:19:08', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(9, 'Shyam G', 'g.shyam@gmail.com', 'Bangalore', 'Abbott', NULL, NULL, '2021/04/24 19:19:31', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(10, 'sreevatsa n s', 'nadig33@gmail.com', 'shimoga', 'Sahyadri Narayana Multispeciality Hospital', NULL, NULL, '2021/04/24 19:19:51', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(11, 'HARIKRISHNAN S', 'drharikrishnan@outlook.com', 'THIRUVANANTHAPURAM', 'Sree Chitra Institute for Medical Sciences and Technology', NULL, NULL, '2021/04/24 19:20:41', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(12, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021/04/24 19:22:42', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(13, 'BIKASH MAJUMDER', 'bikashmaj@gmail.com', 'Kolkata', 'Apollo Gleneagles Hospitals', NULL, NULL, '2021/04/24 19:25:55', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(14, 'Dr Sasirekha', 'drsasirekha@gmail.com', 'Mysore', 'Columbia Asia Hospital', NULL, NULL, '2021/04/24 19:27:08', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(15, 'Bikash Majumder', 'bikashmaj@gmail.com', 'Kolkata', 'Apollo GLENEAGLES HOSPITALS', NULL, NULL, '2021/04/24 19:29:52', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(16, 'Jagadesh Kalathil', 'jagadeesh.kalathil@abbott.com', 'Bangalore East', 'AV', NULL, NULL, '2021/04/24 19:30:31', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(17, 'hema raveesh', 'hemaraveesh@yahoo.com', 'mysore', 'sjic, Mysore', NULL, NULL, '2021/04/24 19:30:31', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(18, 'BIKASH MAJUMDER', 'bikashmaj@gmail.com', 'Kolkata', 'Apollo Gleneagles Hospitals', NULL, NULL, '2021/04/24 19:33:12', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(19, 'ranjan', 'ranjan.shetty@gmail.com', 'Bangalore', 'manipal hospital', NULL, NULL, '2021/04/24 19:36:15', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(20, 'SANJAY BHAT', 'sms.bhat@gmail.com', 'BANGALORE', 'ASTER CMI', NULL, NULL, '2021/04/24 19:42:00', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(21, 'SANJAY BHAT', 'sms.bhat@gmail.com', 'BANGALORE', 'ASTER CMI', NULL, NULL, '2021/04/24 19:45:05', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(22, 'Sreevatsa N S', 'nadig33@gmail.com', 'Shimoga', 'Nh', NULL, NULL, '2021/04/24 21:31:00', '2021-04-24', '2021-04-24', 1, 'Abbott'),
(23, 'sreevatsa n s', 'nadig33@gmail.com', 'shimoga', 'Sahyadri Narayana Multispeciality Hospital', NULL, NULL, '2021/04/24 21:32:49', '2021-04-24', '2021-04-24', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(11) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty_new`
--

INSERT INTO `faculty_new` (`id`, `Name`, `EmailID`, `City`, `Hospital`, `Last Login On`, `Logout Times`) VALUES
(2, 'Goswami, Kalyan', 'Joined before', 'Bangalore ', 'AV ', '4/24/2021, 6:30:56 PM', '4/24/2021, 9:15:31 PM'),
(3, 'Goswami, Kalyan', 'Joined', 'Bangalore ', 'AV ', '4/24/2021, 9:16:34 PM', '4/24/2021, 9:35:55 PM'),
(4, 'Nizar (Guest)', 'Joined', 'Bangalore', 'Abbott', '4/24/2021, 6:55:59 PM', '4/24/2021, 9:35:34 PM'),
(5, 'Nelluvelil, Arun', 'Joined', 'BANGALORE', 'Av', '4/24/2021, 7:01:44 PM', '4/24/2021, 7:09:12 PM'),
(6, 'Dr. Sasirekha (Guest)', 'Joined', 'Mysore', 'Columbia Asia Hospital', '4/24/2021, 7:04:42 PM', '4/24/2021, 7:26:26 PM'),
(7, 'Banerjee, Atanu', 'Joined', 'Kolkata', 'AV', '4/24/2021, 7:18:44 PM', '4/24/2021, 7:57:39 PM'),
(8, 'dr sreevatsa (Guest)', 'Joined', 'shimoga', 'Sahyadri Narayana Multispeciality Hospital', '4/24/2021, 7:20:36 PM', '4/24/2021, 8:27:32 PM'),
(9, 'Prof. Harikrishnan S (Guest)', 'Joined', 'THIRUVANANTHAPURAM', 'Sree Chitra Institute for Medical Sciences and Technology', '4/24/2021, 7:22:28 PM', '4/24/2021, 9:35:20 PM'),
(10, 'Shyam G Nair (Guest)', 'Joined', 'Bangalore', 'Abbott', '4/24/2021, 7:23:08 PM', '4/24/2021, 7:24:16 PM'),
(11, '\\Dr Vivekanand (Guest)\\\"\"', 'Joined', 'DHARWAD', 'SDM Narayana Heart centre', '4/24/2021, 7:26:23 PM', '4/24/2021, 9:35:20 PM'),
(12, 'Dr. Sasirekha (Guest)', 'Joined', 'Mysore', 'Columbia Asia Hospital', '4/24/2021, 7:27:30 PM', '4/24/2021, 9:35:26 PM'),
(13, 'BISHWADYA  MAJUMDER', 'Joined', 'Kolkata', 'Apollo GLENEAGLES HOSPITALS', '4/24/2021, 7:30:59 PM', '4/24/2021, 7:32:27 PM'),
(14, 'BISHWADYA  MAJUMDER', 'Joined', 'Kolkata', 'Apollo GLENEAGLES HOSPITALS', '4/24/2021, 7:33:32 PM', '4/24/2021, 7:36:52 PM'),
(15, 'chinmai raveesh (Guest)', 'Joined', 'mysore', 'sjic, Mysore', '4/24/2021, 7:32:09 PM', '4/24/2021, 9:35:21 PM'),
(16, 'Jagadeesh Kalathil (Guest)', 'Joined', 'Bangalore East', 'AV', '4/24/2021, 7:33:06 PM', '4/24/2021, 9:36:36 PM'),
(17, 'Hema Raveesh (Guest)', 'Joined', 'mysore', 'sjic, Mysore', '4/24/2021, 7:35:44 PM', '4/24/2021, 7:41:39 PM'),
(18, 'ranjan shetty (Guest)', 'Joined', 'Bangalore', 'manipal hospital', '4/24/2021, 7:36:48 PM', '4/24/2021, 9:35:19 PM'),
(19, 'Bikash Majumder (Guest)', 'Joined', 'Kolkata', 'Apollo Gleneagles Hospitals', '4/24/2021, 7:37:34 PM', '4/24/2021, 9:35:16 PM'),
(20, 'SANJAY BHAT (Guest)', 'Joined', 'BANGALORE', 'ASTER CMI', '4/24/2021, 7:43:11 PM', '4/24/2021, 7:44:46 PM'),
(21, 'SANJAY BHAT (Guest)', 'Joined', 'BANGALORE', 'ASTER CMI', '4/24/2021, 7:45:22 PM', '4/24/2021, 9:36:33 PM'),
(22, 'dr sreevatsa (Guest)', 'Joined', 'shimoga', 'Sahyadri Narayana Multispeciality Hospital', '4/24/2021, 8:28:09 PM', '4/24/2021, 9:02:45 PM'),
(23, '\\dr sreevatsa (Guest)\\\"\"', 'Joined', 'shimoga', 'Sahyadri Narayana Multispeciality Hospital', '4/24/2021, 9:03:27 PM', '4/24/2021, 9:30:10 PM'),
(24, '\\\\\\\\\"dr sreevatsa (Guest)\\\\\\\"\\\"\"', 'Joined', 'shimoga', 'Sahyadri Narayana Multispeciality Hospital', '4/24/2021, 9:33:25 PM', '4/24/2021, 9:35:26 PM');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'LOKESH PRAMOD CHAUDHARI', 'lokeshataipg@rediffmail.com', 'When will it start ?', '2021-04-24 19:35:17', 'Abbott', 0, 0),
(2, 'LOKESH PRAMOD CHAUDHARI', 'lokeshataipg@rediffmail.com', 'In case 1. Shouldnt the PTCA to RCA be done first and then FFR be done to LAD and lcx ??', '2021-04-24 19:50:12', 'Abbott', 0, 0),
(3, 'waleem', 'waleempasha@gmail.com', 'Is Ffr in culprit vessel in acute mi validated??', '2021-04-24 20:10:36', 'Abbott', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'Kalyan Goswami', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'AV ', NULL, NULL, '2021-04-23 15:20:13', '2021-04-23 15:20:13', '2021-04-23 16:50:13', 0, 'Abbott', 'a79ab32408b49ab041dc5fb7c6782342'),
(2, 'Kalyan Goswami', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'AV ', NULL, NULL, '2021-04-23 15:20:49', '2021-04-23 15:20:49', '2021-04-23 16:50:49', 0, 'Abbott', 'cecaa4325aa4ee5e2c32ac43bba56374'),
(3, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-23 15:30:11', '2021-04-23 15:30:11', '2021-04-23 15:31:57', 0, 'Abbott', '54862ee573db322899b2fa0c0e3d7c40'),
(4, 'Sanjay Porwal', 'drsanjayporwal@gmail.com', 'Belgaum ', 'KLES ', NULL, NULL, '2021-04-23 21:34:08', '2021-04-23 21:34:08', '2021-04-23 23:04:08', 0, 'Abbott', '517a29ce5d44a518ad52f4a6faf0dada'),
(5, 'Dr Ravi', 'ravicardiac@gmail.com', 'Bengaluru', 'Jayadeva', NULL, NULL, '2021-04-24 08:05:05', '2021-04-24 08:05:05', '2021-04-24 09:35:05', 0, 'Abbott', '3d96fc2906939829cb98ccd1f9af6ca7'),
(6, 'Kumar', 'kumarkallur@gmail.com', 'Bangluru', 'HCG', NULL, NULL, '2021-04-24 08:55:09', '2021-04-24 08:55:09', '2021-04-24 10:25:09', 0, 'Abbott', '18d260790c7ff3f2dff1a36f6be2286c'),
(7, 'Murali M', 'murali13.manohar@gmail.com', 'Bangalore', 'Bangalore Baptist hospital', NULL, NULL, '2021-04-24 10:20:41', '2021-04-24 10:20:41', '2021-04-24 11:50:41', 0, 'Abbott', '0bcce7cc22104a09a79dceb5b0fa2e0a'),
(8, 'Murali M', 'murali13.manohar@gmail.com', 'Bangalore', 'Bangalore Baptist hospital', NULL, NULL, '2021-04-24 10:21:12', '2021-04-24 10:21:12', '2021-04-24 11:51:12', 0, 'Abbott', '838157c9abdb7d8fcc0ec0df4d53971c'),
(9, 'Sharath', 'drsharathps86@gmail.com', 'Shimoga', 'Snmh shimoga', NULL, NULL, '2021-04-24 12:22:57', '2021-04-24 12:22:57', '2021-04-24 13:52:57', 0, 'Abbott', '530a3c7e7e3baf7c409665e8b153b576'),
(10, 'Rajendra', 'drrajendraph@gmail.com', 'Bangalore', 'Jayadeva hospital ', NULL, NULL, '2021-04-24 16:54:46', '2021-04-24 16:54:46', '2021-04-24 16:54:52', 0, 'Abbott', '99c8f564cf14a5af842f563a50b6180d'),
(11, 'Dr sunil Christopher ', 'drsunilchristophert@gmail.com', 'Bangalore', 'Sjic', NULL, NULL, '2021-04-24 17:02:21', '2021-04-24 17:02:21', '2021-04-24 18:32:21', 0, 'Abbott', 'e52f3087b9ca6d37d5efec87750bce39'),
(12, 'Dr Ashith Shettian', 'shettianashith@yahoo.com', 'Mangalore ', 'Father Muller Medical College Hospital ', NULL, NULL, '2021-04-24 17:09:15', '2021-04-24 17:09:15', '2021-04-24 18:39:15', 0, 'Abbott', '54b23ffa359f8b2886e59447e5dd2869'),
(13, 'Hema', 'hemaraveesh@yahoo.com', 'mysore', 'sjic, Mysore', NULL, NULL, '2021-04-24 18:00:44', '2021-04-24 18:00:44', '2021-04-24 19:30:44', 0, 'Abbott', '69b2b38ace59e67e8c5e023d20d976ac'),
(14, 'Arpit Jain', 'jarpit89@gmail.com', 'Kampoo, Gwalior', 'Kmc', NULL, NULL, '2021-04-24 18:02:11', '2021-04-24 18:02:11', '2021-04-24 19:32:11', 0, 'Abbott', 'd67de44050f1c7e6848f387f6db1e7f0'),
(15, 'Kalyan Goswami ', 'kalyan.goswami@abbott.com', 'Bangalore ', 'AV ', NULL, NULL, '2021-04-24 18:25:05', '2021-04-24 18:25:05', '2021-04-24 19:55:05', 0, 'Abbott', 'f5e260a03a882234eb11c4a4af5cf4a5'),
(16, 'Jinto vj', 'jintovj2008@gmail.com', 'Banglore ', 'Banglore hospital', NULL, NULL, '2021-04-24 19:03:02', '2021-04-24 19:03:02', '2021-04-24 20:33:02', 0, 'Abbott', '13a6668029f24c23f5e2d885b6bf0035'),
(17, 'V', 'mahi@gmail.com', 'Hh', 'Bj', NULL, NULL, '2021-04-24 19:05:39', '2021-04-24 19:05:39', '2021-04-24 20:35:39', 0, 'Abbott', '61bcd9269092a0211e4e5ee727389947'),
(18, 'Prabhavathi ', 'prabhavathi_in@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-04-24 19:06:23', '2021-04-24 19:06:23', '2021-04-24 20:36:23', 0, 'Abbott', '0670be2331ed4e762b5fae5ea0847ddd'),
(19, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-04-24 19:08:45', '2021-04-24 19:08:45', '2021-04-24 20:43:07', 0, 'Abbott', 'dc5097b62f3195876dd9067558e05eb3'),
(20, 'Deepthi Mangesh', 'drgrdeepthimangesh158@gmail.com', 'Mysore', 'Columbia ', NULL, NULL, '2021-04-24 19:10:18', '2021-04-24 19:10:18', '2021-04-24 20:40:18', 0, 'Abbott', 'bed5ae5c48d324e76254c951abcdebe8'),
(21, 'Akshat Jharia', 'akshatjharia@gmail.com', 'Bangalore', 'COACT', NULL, NULL, '2021-04-24 19:12:31', '2021-04-24 19:12:31', '2021-04-24 20:42:31', 0, 'Abbott', '5b8c281f16639186c6c3cc13d1ccf9d8'),
(22, 'LOKESH PRAMOD CHAUDHARI', 'lokeshataipg@rediffmail.com', 'banglore', 'Sjic', NULL, NULL, '2021-04-24 19:15:04', '2021-04-24 19:15:04', '2021-04-24 20:45:04', 0, 'Abbott', 'b4a09013e6df328c3647c2d46262aa67'),
(23, 'Hema', 'hemaraveesh@yahoo.com', 'mysore', 'sjic, Mysore', NULL, NULL, '2021-04-24 19:17:20', '2021-04-24 19:17:20', '2021-04-24 20:47:20', 0, 'Abbott', '66574c4db242cda6994fe7e57c57cd93'),
(24, 'Dr. Parthiban G', 'parthi2012@gmail.com', 'Bangalore', 'Jayadeva', NULL, NULL, '2021-04-24 19:19:30', '2021-04-24 19:19:30', '2021-04-24 20:49:30', 0, 'Abbott', 'b040b19972540b8c448cc0314c2dbd5f'),
(25, 'lokesh', 'drlokeshpc@gmail.com', 'bengaluru', 'sjicr', NULL, NULL, '2021-04-24 19:21:50', '2021-04-24 19:21:50', '2021-04-24 20:51:50', 0, 'Abbott', '2a2c9ed0f35ba7f9ac84da69c7e38744'),
(26, 'Thabit Ahmed', 'thabitahmed413@gmail.com', 'Bangalore', 'Baptist hospital', NULL, NULL, '2021-04-24 19:21:58', '2021-04-24 19:21:58', '2021-04-24 20:51:58', 0, 'Abbott', '87c3abaec30dd75b72ed4ead91dc74b7'),
(27, 'Dr sunil Christopher ', 'drsunilchristophert@gmail.com', 'Bangalore', 'Sjic', NULL, NULL, '2021-04-24 19:24:46', '2021-04-24 19:24:46', '2021-04-24 20:54:46', 0, 'Abbott', '8b6cbba92e5c346e2d6ea6a0815795a3'),
(28, 'Radhakrishnan k ', 'rk.majalla@gmail.com', 'Banglore', 'Columbia Asia hospital', NULL, NULL, '2021-04-24 19:25:23', '2021-04-24 19:25:23', '2021-04-24 20:55:23', 0, 'Abbott', '596fb84df993c8a8f1c1bc21742222c6'),
(29, 'Ravi math', 'ravismath76@gmail.com', 'Bangalore', 'Jayadeva hospital', NULL, NULL, '2021-04-24 19:26:55', '2021-04-24 19:26:55', '2021-04-24 20:56:55', 0, 'Abbott', '19d7d9203ea7d2f5b5fbf8dfb6d12774'),
(30, 'Abhilash', 'abhilash.mohanan@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-04-24 19:28:34', '2021-04-24 19:28:34', '2021-04-24 20:58:34', 0, 'Abbott', 'acad9307a280d4e5dcd18b63c40fe82e'),
(31, 'veenu john', 'veenu.remaliya@gmail.com', 'Mysore', 'Apollo hospital ', NULL, NULL, '2021-04-24 19:28:56', '2021-04-24 19:28:56', '2021-04-24 20:58:56', 0, 'Abbott', '3b2ccb69c4e1399c1e0c2eb5901b911f'),
(32, 'Nuthan', 'nuthanmohite@gmail.com', 'Bangalore', 'Apollo', NULL, NULL, '2021-04-24 19:30:34', '2021-04-24 19:30:34', '2021-04-24 21:00:34', 0, 'Abbott', '8d090d86584f46c6d0d107cc201dcae9'),
(33, 'Arun James', 'arun.nelluvelil@abbott.com', 'Bangalore', 'Av', NULL, NULL, '2021-04-24 19:31:08', '2021-04-24 19:31:08', '2021-04-24 21:01:08', 0, 'Abbott', 'f1ebdf214cba9785c5600bd33c5d848d'),
(34, 'Sudheesh Chandran', 'sudheesh.chandran@abbott.com', 'Trivandrum ', 'Abbott healthcare pvt ltd', NULL, NULL, '2021-04-24 19:31:10', '2021-04-24 19:31:10', '2021-04-24 21:01:10', 0, 'Abbott', 'c3dd1656ec3a5977e2e413eff41e92cf'),
(35, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-04-24 19:31:29', '2021-04-24 19:31:29', '2021-04-24 21:01:29', 0, 'Abbott', '2c8cad92a8a16b66b611f55f4029f93e'),
(36, 'Nagamani A C', 'drnagamani_c@yahoo.co.in', 'Bengaluru', 'Jayadeva hospital ', NULL, NULL, '2021-04-24 19:31:33', '2021-04-24 19:31:33', '2021-04-24 21:01:33', 0, 'Abbott', 'b398427ca8a1ae863dd0d6b3e5945f28'),
(37, 'chirag d', 'chiragnichi@gmail.com', 'bangalore', 'fortis', NULL, NULL, '2021-04-24 19:31:37', '2021-04-24 19:31:37', '2021-04-24 21:01:37', 0, 'Abbott', 'c4e2d2d722a3954e4a57f720852803e5'),
(38, 'Arun James', 'arun.nelluvelil@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-04-24 19:32:16', '2021-04-24 19:32:16', '2021-04-24 21:02:16', 0, 'Abbott', '287edc82850cfabc80077c4f6ae41a9a'),
(39, 'Amith R', 'amith1ind@gmail.com', 'Bangalore ', 'SJICR', NULL, NULL, '2021-04-24 19:32:43', '2021-04-24 19:32:43', '2021-04-24 21:02:43', 0, 'Abbott', '7c1e15f9ae82cb79d16ff69786c1403b'),
(40, 'LOKESH PRAMOD CHAUDHARI', 'lokeshataipg@rediffmail.com', 'banglore', 'Sjic', NULL, NULL, '2021-04-24 19:32:50', '2021-04-24 19:32:50', '2021-04-24 21:02:50', 0, 'Abbott', '3ec73d883944031fc50328b8f33c4619'),
(41, 'waleem', 'waleempasha@gmail.com', 'Mangaluru ', 'Kmc hospital ', NULL, NULL, '2021-04-24 19:33:41', '2021-04-24 19:33:41', '2021-04-24 19:38:30', 0, 'Abbott', '80d4ec67ed88b4f0babdae03070a83db'),
(42, 'Sunil Roy', 'sunil.roy@abbott.com', 'Bhubaneswar', 'AV', NULL, NULL, '2021-04-24 19:33:51', '2021-04-24 19:33:51', '2021-04-24 21:03:51', 0, 'Abbott', '740c562c98a1cf58326435c3b91bc72c'),
(43, 'chaudappa ', 'shakapurcj@gmail.com', 'DHARWAD', 'sdmnh', NULL, NULL, '2021-04-24 19:33:52', '2021-04-24 19:33:52', '2021-04-24 21:03:52', 0, 'Abbott', '7238957de29a9ccfca282ee2e36eb7c7'),
(44, 'Dr Sunil Kumar S', 'sunilbmc98@gmail.com', 'Bangalore', 'Columbia Asia Hospital', NULL, NULL, '2021-04-24 19:33:53', '2021-04-24 19:33:53', '2021-04-24 21:03:53', 0, 'Abbott', '565e0d02dfabdd8d4b5e621ca27a9f34'),
(45, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 19:34:15', '2021-04-24 19:34:15', '2021-04-24 21:04:15', 0, 'Abbott', '8dd179ca7eca9d1a06d6790b39a43da7'),
(46, 'Balaraj', 'balarajud2000@gmail.com', 'Bangalore ', 'Sjicr', NULL, NULL, '2021-04-24 19:34:41', '2021-04-24 19:34:41', '2021-04-24 21:04:41', 0, 'Abbott', 'bf9cffc4708c8f1bec8fa185cea05748'),
(47, 'Prabhavathi ', 'prabhavathi_in@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-04-24 19:35:19', '2021-04-24 19:35:19', '2021-04-24 19:41:39', 0, 'Abbott', '31916edeab87bd8959b85e2c5b29abe9'),
(48, 'Mahendra ', 'mahendrakumar.g@abbott.com', 'Mangaluru', 'Abbott', NULL, NULL, '2021-04-24 19:36:03', '2021-04-24 19:36:03', '2021-04-24 21:06:03', 0, 'Abbott', '05f3c42829502f2e19c0f32b1cd5c2d9'),
(49, 'LOKESH PRAMOD CHAUDHARI', 'lokeshataipg@rediffmail.com', 'banglore', 'Sjic', NULL, NULL, '2021-04-24 19:36:07', '2021-04-24 19:36:07', '2021-04-24 21:06:07', 0, 'Abbott', '108e62edda0f6141a2752b64cebbc14e'),
(50, 'Shouvik Bhattacharya', 'shouvik.bhattacharya@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021-04-24 19:36:54', '2021-04-24 19:36:54', '2021-04-24 21:06:54', 0, 'Abbott', '14941c1244e0a0708e8f21c3401f2265'),
(51, 'Dr Jayanta Saha ', 'drjayanta.saha2010@gmail.com', 'Kolkata', 'Calcutta Medical College ', NULL, NULL, '2021-04-24 19:37:39', '2021-04-24 19:37:39', '2021-04-24 21:07:39', 0, 'Abbott', '3a4a9f0cef30cb14efaa97f77738693e'),
(52, 'Spandana ', 'Spandana.komma1@gmail.com', 'Cuddapah', 'Jayadeva hospital ', NULL, NULL, '2021-04-24 19:38:22', '2021-04-24 19:38:22', '2021-04-24 21:08:22', 0, 'Abbott', '0015a003b4d638e78319bbdc7d6ceb6e'),
(53, 'Nagamani AC', 'drnagamani_c@yahoo.co.in', 'Bengaluru', 'Jayadeva', NULL, NULL, '2021-04-24 19:38:24', '2021-04-24 19:38:24', '2021-04-24 21:08:24', 0, 'Abbott', 'd1a79180f3cfd9c6eeae70fee7674c2c'),
(54, 'Arunkumar ', 'a.rubesh@gmail.com', 'Bangalore ', 'SJICR ', NULL, NULL, '2021-04-24 19:38:25', '2021-04-24 19:38:25', '2021-04-24 21:08:25', 0, 'Abbott', 'e22075fdddb3ff34215f62c4c55c6396'),
(55, 'Snehal Paul', 'paul_snehal@yahoo.com', 'BANGALORE', 'Fortis hospital Cunningham road Bangalore', NULL, NULL, '2021-04-24 19:38:50', '2021-04-24 19:38:50', '2021-04-24 21:08:50', 0, 'Abbott', '02dbd5e23ebfca0637df1fa329c675cf'),
(56, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 19:39:55', '2021-04-24 19:39:55', '2021-04-24 21:09:55', 0, 'Abbott', '5d765144186916b53ff293c711c6c7e4'),
(57, 'Dr K C Narzary', 'drkrishna_narzary@yahoo.co.in', 'Guwahati', 'GNRC', NULL, NULL, '2021-04-24 19:39:58', '2021-04-24 19:39:58', '2021-04-24 21:09:58', 0, 'Abbott', '6820917b3cbbe19fbca7cd9bae306d61'),
(58, 'Dr Shashank Baruah', 'shashank.baruah@gmail.com', 'Guwahati', 'Healthcity Hospital', NULL, NULL, '2021-04-24 19:40:01', '2021-04-24 19:40:01', '2021-04-24 21:10:01', 0, 'Abbott', 'c9a273ebd8ba868d3386654b76acf697'),
(59, 'Dr Chandan Modak', 'chandan.dr.modak@yahoo.com', 'Guwahati', 'Dispur Hospital', NULL, NULL, '2021-04-24 19:40:03', '2021-04-24 19:40:03', '2021-04-24 21:10:03', 0, 'Abbott', '6b0d99e2090635c69cee4f90dd4b9771'),
(60, 'Dr Arijit Ghosh ', 'arijitjoy@gmail.com', 'Kolkata', 'AMRI Salt Lake ', NULL, NULL, '2021-04-24 19:40:19', '2021-04-24 19:40:19', '2021-04-24 21:10:19', 0, 'Abbott', 'b0f19949f22eeddd90a6ff33a40777e2'),
(61, 'Slomi', 'slomi.gupta@gmail.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-04-24 19:40:24', '2021-04-24 19:40:24', '2021-04-24 19:53:37', 0, 'Abbott', '13b5ff0f41496cf034cfcdacbcec05c1'),
(62, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 19:40:32', '2021-04-24 19:40:32', '2021-04-24 19:42:44', 0, 'Abbott', 'ceb527e4b212fa11f5fea4ba3a5afb38'),
(63, 'Mohan', 'drmohankhn@gmail.com', 'Bangalore ', 'CAHSR ', NULL, NULL, '2021-04-24 19:40:53', '2021-04-24 19:40:53', '2021-04-24 21:10:53', 0, 'Abbott', 'a8d8858d700576eb09c951e4ad82735f'),
(64, 'Dr Aritra Konar ', 'konar1978.dr@gmail.com', 'Kolkata', 'Apollo Gleneagles ', NULL, NULL, '2021-04-24 19:40:53', '2021-04-24 19:40:53', '2021-04-24 21:10:53', 0, 'Abbott', '51cba60bee7565e49a9d96170e5360d8'),
(65, 'Arunkumar', 'a.rubesh@gmail.com', 'Bangalore ', 'SJICR', NULL, NULL, '2021-04-24 19:41:04', '2021-04-24 19:41:04', '2021-04-24 21:11:04', 0, 'Abbott', '56a7a234f05f5497e89fa3898dcfce11'),
(66, 'Dr Asfaque Ahmed ', 'ahmed.asfaque@gmail.com', 'Kolkata', 'S S Chatterjee Hospital ', NULL, NULL, '2021-04-24 19:41:14', '2021-04-24 19:41:14', '2021-04-24 21:11:14', 0, 'Abbott', '110577da2a1f1075bc190f2a06afa25e'),
(67, 'Dr Keshavamurthy', 'cbkeshavamurthy@gmail.com', 'Mysore', 'Columbia Asia Hospital', NULL, NULL, '2021-04-24 19:41:15', '2021-04-24 19:41:15', '2021-04-24 21:11:15', 0, 'Abbott', 'd9a4acb8d87a380304b93225fc26f370'),
(68, 'Dr Asraful Haque ', 'haque.asraful@gmail.com', 'Kolkata', 'S S Chatterjee Hospital ', NULL, NULL, '2021-04-24 19:41:35', '2021-04-24 19:41:35', '2021-04-24 21:11:35', 0, 'Abbott', '7f9fdbdde59c6e86ffe09543f149e626'),
(69, 'Dr Nabarun Roy', 'nabarun.roy@gmail.com', 'Kolkata', 'Apollo Gleneagles ', NULL, NULL, '2021-04-24 19:41:53', '2021-04-24 19:41:53', '2021-04-24 21:11:53', 0, 'Abbott', 'b8023fd051aff0a8157bcedf2ce008cf'),
(70, 'Shadman', 'drshadmanali@gmail.com', 'Bengaluru', 'SJICR', NULL, NULL, '2021-04-24 19:41:57', '2021-04-24 19:41:57', '2021-04-24 21:11:57', 0, 'Abbott', '4a22f6a3469b96eaa275148f17f5f7fa'),
(71, 'Prabhavathi ', 'prabhavathi_in@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-04-24 19:42:15', '2021-04-24 19:42:15', '2021-04-24 19:48:24', 0, 'Abbott', '8d5071f58c333edea4c87f94d3ab4a7e'),
(72, 'Dr Subhashis Roychowdhury ', 'drsubhasis.roychowdhury@yahoo.com', 'Kolkata', 'AMRI Salt Lake ', NULL, NULL, '2021-04-24 19:42:19', '2021-04-24 19:42:19', '2021-04-24 21:12:19', 0, 'Abbott', '85ce323b0ca20f81e55ef7eee4fd5424'),
(73, 'Dr Raja Nag ', 'drrajanag@gmail.com', 'Kolkata', 'Apollo Hospital Kolkata', NULL, NULL, '2021-04-24 19:42:48', '2021-04-24 19:42:48', '2021-04-24 21:12:48', 0, 'Abbott', '8ab403df38e42ee47888469b05584d22'),
(74, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 19:42:56', '2021-04-24 19:42:56', '2021-04-24 21:12:56', 0, 'Abbott', '9695a632ee9ae1cea58ce842bb545be5'),
(75, 'Nagamani A C', 'drnagamani_c@yahoo.co.in', 'Bengaluru', 'Jayadeva hospital ', NULL, NULL, '2021-04-24 19:43:09', '2021-04-24 19:43:09', '2021-04-24 20:08:54', 0, 'Abbott', '528a3588139b8cc936c8b8dcb2dea662'),
(76, 'Dr Sachit Majumder ', 'Sachit.majumder@gmail.com', 'Kolkata', 'Apollo Gleneagles ', NULL, NULL, '2021-04-24 19:43:23', '2021-04-24 19:43:23', '2021-04-24 21:13:23', 0, 'Abbott', 'abefe7cc12292d6e7a0706ebe7adbda2'),
(77, 'Kuldeep', 'krtworld@gmail.com', 'Bangalore 560069', 'SJICR', NULL, NULL, '2021-04-24 19:43:41', '2021-04-24 19:43:41', '2021-04-24 19:56:50', 0, 'Abbott', '5c2a0285eac8ff4fd0ab81da595df261'),
(78, 'Arti', 'drartibaheti@gmail.com', 'Banglore ', 'Fortis', NULL, NULL, '2021-04-24 19:43:46', '2021-04-24 19:43:46', '2021-04-24 21:13:46', 0, 'Abbott', '0642ed600ffab845360bcc695e6dd71e'),
(79, 'Dr S S Das ', 'das.s@gmail.com', 'Kolkata', 'Apollo Hospital Kolkata', NULL, NULL, '2021-04-24 19:43:57', '2021-04-24 19:43:57', '2021-04-24 21:13:57', 0, 'Abbott', 'c781f74ed9826c2c1a0991fa331563b9'),
(80, 'Dr Swapan Kumar Dey ', 'dey.swapan@gmail.com', 'Kolkata', 'Apollo Gleneagles ', NULL, NULL, '2021-04-24 19:44:27', '2021-04-24 19:44:27', '2021-04-24 21:14:27', 0, 'Abbott', '3abf44cd8afbab7cb05f0bca8ea1527c'),
(81, 'Dr Debasis Ghosh ', 'drdebasisg@gmail.com', 'Kolkata', 'Apollo Hospital Kolkata', NULL, NULL, '2021-04-24 19:44:59', '2021-04-24 19:44:59', '2021-04-24 21:14:59', 0, 'Abbott', '9fc8b32675ba2175faeed521880a5a5f'),
(82, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 19:45:11', '2021-04-24 19:45:11', '2021-04-24 21:15:11', 0, 'Abbott', '435db55725ee6f4e7da23715e94d006f'),
(83, 'Mr Bala N Subramani ', 'bala.subramani@gmail.com', 'Kolkata', 'Apollo Kolkata ', NULL, NULL, '2021-04-24 19:45:22', '2021-04-24 19:45:22', '2021-04-24 21:15:22', 0, 'Abbott', '689ee7b19c2340294cac900704b5b431'),
(84, 'Dr. Chetana ', 'chetana88@gmail.com', 'Mysore ', 'SJICSR', NULL, NULL, '2021-04-24 19:45:25', '2021-04-24 19:45:25', '2021-04-24 21:15:25', 0, 'Abbott', '36980387aeccf5d05141e597392d4b34'),
(85, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 19:46:08', '2021-04-24 19:46:08', '2021-04-24 19:50:36', 0, 'Abbott', '16772a2a05006d031e35e14181bb14df'),
(86, 'Santanu Bag', 'bag.santanu@yahoo.com', 'Kolkata', 'Apollo Hospital Kolkata', NULL, NULL, '2021-04-24 19:46:17', '2021-04-24 19:46:17', '2021-04-24 21:16:17', 0, 'Abbott', '321d028762c995e1ed0b670dd2a18e3f'),
(87, 'Prabhavathi ', 'prabhavathi_in@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-04-24 19:48:50', '2021-04-24 19:48:50', '2021-04-24 21:18:50', 0, 'Abbott', '6b5f55f8f1ef811bbbbe1095ae2eea2e'),
(88, 'LOKESH PRAMOD CHAUDHARI', 'lokeshataipg@rediffmail.com', 'banglore', 'Sjic', NULL, NULL, '2021-04-24 19:49:18', '2021-04-24 19:49:18', '2021-04-24 21:19:18', 0, 'Abbott', '8b60b366ccbf155fd8c7c7862453070b'),
(89, 'Spandana ', 'Spandana.komma1@gmail.com', 'Cuddapah', 'Jayadeva hospital ', NULL, NULL, '2021-04-24 19:50:48', '2021-04-24 19:50:48', '2021-04-24 21:20:48', 0, 'Abbott', 'f032dd603292bf84c8d6cd96a9c316b1'),
(90, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 19:50:48', '2021-04-24 19:50:48', '2021-04-24 19:51:59', 0, 'Abbott', 'bc14ad132062c042691339fd1fcb7e7a'),
(91, 'Kk', 'kkk@gmail.com', 'Blore', 'SJIC', NULL, NULL, '2021-04-24 19:51:18', '2021-04-24 19:51:18', '2021-04-24 21:21:18', 0, 'Abbott', 'ff6d8a5c4b6ef909aa8f12c77aeb79ba'),
(92, 'Dr Pradeep Kumar K ', 'doctorpradeep@gmail.com', 'Bengaluru/Bangalore', 'Sapthagiri hospitals ', NULL, NULL, '2021-04-24 19:51:20', '2021-04-24 19:51:20', '2021-04-24 21:21:20', 0, 'Abbott', 'f8774f6ddfd1193d8f0fd6a45b34f3bf'),
(93, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 19:52:22', '2021-04-24 19:52:22', '2021-04-24 19:55:05', 0, 'Abbott', '50049c476ceefe93ced0243c58095fea'),
(94, 'Kalyan ', 'kalyan.goswami@abbott.com', 'Bangalore ', 'AB', NULL, NULL, '2021-04-24 19:52:26', '2021-04-24 19:52:26', '2021-04-24 21:22:26', 0, 'Abbott', '8101156e76c048a47ce352ce666dd016'),
(95, 'DR.Satish.L', 'satish.lingaraju@gmail.com', 'Bangalore', 'Sparsh hospital ', NULL, NULL, '2021-04-24 19:52:39', '2021-04-24 19:52:39', '2021-04-24 21:22:39', 0, 'Abbott', 'eb5e06a72f277d4c490d51c451323435'),
(96, 'Reshma', 'reshmausman30@yahoo.com', 'MYSURU', 'Columbia asia hospital myzore', NULL, NULL, '2021-04-24 19:52:55', '2021-04-24 19:52:55', '2021-04-24 21:22:55', 0, 'Abbott', 'e8cf178c3b629ceada641b3225d6ec65'),
(97, 'veenu john', 'veenu.remaliya@gmail.com', 'Mysore', 'Apollo hospital ', NULL, NULL, '2021-04-24 19:54:36', '2021-04-24 19:54:36', '2021-04-24 21:24:36', 0, 'Abbott', 'c1b945ee85e5416f85528d275f44e15f'),
(98, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 19:55:15', '2021-04-24 19:55:15', '2021-04-24 19:56:37', 0, 'Abbott', '21d7621c843fa6f23543fcda4b2e2cf7'),
(99, 'Sachit Roy', 'sachit.roy@abbott.com', 'Patna', 'AV', NULL, NULL, '2021-04-24 19:55:41', '2021-04-24 19:55:41', '2021-04-24 21:25:41', 0, 'Abbott', '845a1ca2d34d0e6f8a407b3cc8ea45f7'),
(100, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 19:56:51', '2021-04-24 19:56:51', '2021-04-24 19:59:35', 0, 'Abbott', 'a702ff28a5900352b80709f3f22fc8c5'),
(101, 'Dr sanjana ', 'drsanjana.pradeep@gmail.com', 'Mysore', 'Columbia Asia ', NULL, NULL, '2021-04-24 19:57:10', '2021-04-24 19:57:10', '2021-04-24 21:27:10', 0, 'Abbott', '4dd5b54749212d1f8bdb42329e252d60'),
(102, 'Kuldeep', 'krtworld@gmail.com', 'Bangalore 560069', 'SJICR', NULL, NULL, '2021-04-24 19:57:17', '2021-04-24 19:57:17', '2021-04-24 21:27:17', 0, 'Abbott', '4de57df52d8692a44c9ec27da1e2ce61'),
(103, 'Ravi math', 'ravismath76@gmail.com', 'Bangalore', 'Jayadeva hospital', NULL, NULL, '2021-04-24 19:57:46', '2021-04-24 19:57:46', '2021-04-24 21:27:46', 0, 'Abbott', 'b9ef2f35df3c555975820c6fa5ca15ad'),
(104, 'Pavithra L', 'drpavithral@gmail.com', 'BTM 2 nd stage Bangalore', 'Sjicr', NULL, NULL, '2021-04-24 19:57:53', '2021-04-24 19:57:53', '2021-04-24 21:27:53', 0, 'Abbott', '6ab00d5be30a4fa3580104b34e4a5792'),
(105, 'Reshma', 'reshmausman30@yahoo.com', 'MYSURU', 'Columbia asia hospital myzore', NULL, NULL, '2021-04-24 19:58:28', '2021-04-24 19:58:28', '2021-04-24 21:28:28', 0, 'Abbott', '3ceee81579e9ac34eae1d6a063ee67cc'),
(106, 'Reshma', 'reshmausman30@yahoo.com', 'MYSURU', 'Columbia asia hospital myzore', NULL, NULL, '2021-04-24 19:58:30', '2021-04-24 19:58:30', '2021-04-24 21:28:30', 0, 'Abbott', 'a9729651ae2f04a847e4d95d29c57b2d'),
(107, 'SHRIDHARA T R', 'drshrishridhar@gmail.com', 'Mysore', 'Cauvery heart and multispecialty hospital ', NULL, NULL, '2021-04-24 19:58:36', '2021-04-24 19:58:36', '2021-04-24 21:28:36', 0, 'Abbott', 'f0f720c8d2c6c5d34c276e30f9761f33'),
(108, 'SHRIDHARA T R', 'drshrishridhar@gmail.com', 'Mysore', 'Cauvery heart and multispecialty hospital ', NULL, NULL, '2021-04-24 19:58:38', '2021-04-24 19:58:38', '2021-04-24 21:28:38', 0, 'Abbott', '5b902699809ac8912f7fb459c462d14c'),
(109, 'SHRIDHARA T R', 'drshrishridhar@gmail.com', 'Mysore', 'Cauvery heart and multispecialty hospital ', NULL, NULL, '2021-04-24 19:58:39', '2021-04-24 19:58:39', '2021-04-24 21:28:39', 0, 'Abbott', 'b7695df3b092a9e0fa374abc70e925ed'),
(110, 'SHRIDHARA T R', 'drshrishridhar@gmail.com', 'Mysore', 'Cauvery heart and multispecialty hospital ', NULL, NULL, '2021-04-24 19:58:43', '2021-04-24 19:58:43', '2021-04-24 21:28:43', 0, 'Abbott', 'e8f0bd5a0a7490a10854faa592ed9606'),
(111, 'SHRIDHARA T R', 'drshrishridhar@gmail.com', 'Mysore', 'Cauvery heart and multispecialty hospital ', NULL, NULL, '2021-04-24 19:58:46', '2021-04-24 19:58:46', '2021-04-24 21:28:46', 0, 'Abbott', '29dad86d7de48bdf98cf57bdf18f0dc8'),
(112, 'SHRIDHARA T R', 'drshrishridhar@gmail.com', 'Mysore', 'Cauvery heart and multispecialty hospital ', NULL, NULL, '2021-04-24 19:58:54', '2021-04-24 19:58:54', '2021-04-24 21:28:54', 0, 'Abbott', 'cc75a795b095c8dcf8e156ce1dd6b93f'),
(113, 'SHRIDHARA T R', 'drshrishridhar@gmail.com', 'Mysore', 'Cauvery heart and multispecialty hospital ', NULL, NULL, '2021-04-24 19:58:54', '2021-04-24 19:58:54', '2021-04-24 21:28:54', 0, 'Abbott', '43ee7fc9980a9ef626fd9a98abd9fdf1'),
(114, 'SHRIDHARA T R', 'drshrishridhar@gmail.com', 'Mysore', 'Cauvery heart and multispecialty hospital ', NULL, NULL, '2021-04-24 19:58:56', '2021-04-24 19:58:56', '2021-04-24 21:28:56', 0, 'Abbott', '0d563d8b0a4c0be4f42f77debecf675c'),
(115, 'SHRIDHARA T R', 'drshrishridhar@gmail.com', 'Mysore', 'Cauvery heart and multispecialty hospital ', NULL, NULL, '2021-04-24 19:58:58', '2021-04-24 19:58:58', '2021-04-24 21:28:58', 0, 'Abbott', '4ebe0e006deb212cdbc3f31f897c2de6'),
(116, 'Reshma', 'reshmausman30@yahoo.com', 'MYSURU', 'Columbia asia hospital myzore', NULL, NULL, '2021-04-24 19:59:25', '2021-04-24 19:59:25', '2021-04-24 21:29:25', 0, 'Abbott', '5072619aa8beecfba0a9ea169b97d8f4'),
(117, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 19:59:47', '2021-04-24 19:59:47', '2021-04-24 21:29:47', 0, 'Abbott', '5fa67f103066f3d0781bc8e0fd982f15'),
(118, 'Murali M', 'murali13.manohar@gmail.com', 'Bangalore', 'Bangalore Baptist hospital', NULL, NULL, '2021-04-24 20:01:39', '2021-04-24 20:01:39', '2021-04-24 21:31:39', 0, 'Abbott', '7cbbf2c48831afdf4c032eae3166061a'),
(119, 'Sunil Roy', 'sunil.roy@abbott.com', 'Bhubaneswar', 'AV', NULL, NULL, '2021-04-24 20:02:24', '2021-04-24 20:02:24', '2021-04-24 21:32:24', 0, 'Abbott', 'ebd75cac9eb8cb0427b802eb342f70a5'),
(120, 'Reshma', 'reshmausman30@yahoo.com', 'MYSURU', 'Columbia asia hospital myzore', NULL, NULL, '2021-04-24 20:04:53', '2021-04-24 20:04:53', '2021-04-24 21:34:53', 0, 'Abbott', 'b6a3bdf5bef2cf922df6b78a2d428f77'),
(121, 'Praveen kumar', 'jipmer.praveen@gmail.com', 'kolar', 'Fortis', NULL, NULL, '2021-04-24 20:06:29', '2021-04-24 20:06:29', '2021-04-24 21:36:29', 0, 'Abbott', 'e12c5398e68c519f2ea9aa5cd5493074'),
(122, 'waleem', 'waleempasha@gmail.com', 'Mangaluru ', 'Kmc hospital ', NULL, NULL, '2021-04-24 20:06:56', '2021-04-24 20:06:56', '2021-04-24 21:36:56', 0, 'Abbott', '1d1e4c3ce3d338a447175344f9497d96'),
(123, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 20:07:25', '2021-04-24 20:07:25', '2021-04-24 20:10:22', 0, 'Abbott', '75355d846d6dbb9fd32b47d1842ee3fc'),
(124, 'vedprakash verma', 'vedverma13@gmail.com', 'Mangalore ', 'KMCH, manglore', NULL, NULL, '2021-04-24 20:07:50', '2021-04-24 20:07:50', '2021-04-24 21:37:50', 0, 'Abbott', 'ae7deee59ca794f36b8288457e17522a'),
(125, 'Balaraj', 'balarajud2000@gmail.com', 'Bangalore ', 'Sjicr', NULL, NULL, '2021-04-24 20:07:55', '2021-04-24 20:07:55', '2021-04-24 21:37:55', 0, 'Abbott', '44cca442ef839b53d9aa7a95361e7f4a'),
(126, 'Nagamani AC', 'drnagamani_c@yahoo.co.in', 'Bengaluru', 'Jayadeva', NULL, NULL, '2021-04-24 20:08:33', '2021-04-24 20:08:33', '2021-04-24 20:09:31', 0, 'Abbott', '4fbbd808cd64e9ba84f1a4dd45d77e29'),
(127, 'Dr Ashith Shettian', 'shettianashith@yahoo.com', 'Mangalore ', 'FMMCH', NULL, NULL, '2021-04-24 20:08:42', '2021-04-24 20:08:42', '2021-04-24 21:38:42', 0, 'Abbott', 'f799495bff2912c6edffa61d43f91c8e'),
(128, 'Sudheesh Chandran', 'sudheesh.chandran@abbott.com', 'Trivandrum', 'Abbott', NULL, NULL, '2021-04-24 20:08:55', '2021-04-24 20:08:55', '2021-04-24 21:38:55', 0, 'Abbott', '8498e991d666b34c7445cfee239acd9b'),
(129, 'Nagamani A C', 'drnagamani_c@yahoo.co.in', 'Bengaluru', 'Jayadeva hospital ', NULL, NULL, '2021-04-24 20:09:13', '2021-04-24 20:09:13', '2021-04-24 21:39:13', 0, 'Abbott', '2a7042299a97193fbd655c01e9c4bd1c'),
(130, 'Kalyan ', 'kalyan.goswami@abbott.com', 'Bangalore ', 'AV', NULL, NULL, '2021-04-24 20:09:37', '2021-04-24 20:09:37', '2021-04-24 21:39:37', 0, 'Abbott', '65fc44a8c0c30f1061b4bf49671af68b'),
(131, 'Raguraman', 'raguraman.sundar@gmail.com', 'Bangalore', 'Jayadeva institute', NULL, NULL, '2021-04-24 20:10:33', '2021-04-24 20:10:33', '2021-04-24 21:40:33', 0, 'Abbott', '301b882189a9b1d3a3a602d46ef5ace5'),
(132, 'Sudhakar Reddy Pathakota', 'sudhakarreddy.pathakota@gmail.com', 'Hyderabad ', 'Care hospitals ', NULL, NULL, '2021-04-24 20:10:46', '2021-04-24 20:10:46', '2021-04-24 21:40:46', 0, 'Abbott', '9bc408500223fc8ea5f9347261a3f7b6'),
(133, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 20:10:50', '2021-04-24 20:10:50', '2021-04-24 21:40:50', 0, 'Abbott', '375ee11413408a5faf6db5b90922fe58'),
(134, 'Sathish S', 'drsathishsep76@gmail.com', 'Bengaluru', 'Sri jayadeva institute of cardiovascular science ', NULL, NULL, '2021-04-24 20:11:22', '2021-04-24 20:11:22', '2021-04-24 21:41:22', 0, 'Abbott', '25b8e7d16044dc411a34fcf34cbfc3ae'),
(135, 'Pavithra L', 'drpavithral@gmail.com', 'BTM 2 nd stage Bangalore', 'Sjicr', NULL, NULL, '2021-04-24 20:14:07', '2021-04-24 20:14:07', '2021-04-24 21:44:07', 0, 'Abbott', '0ca7d2a2553799f3a16cf19ec38f3cb7'),
(136, 'Pavithra L', 'drpavithral@gmail.com', 'BTM 2 nd stage Bangalore', 'Sjicr', NULL, NULL, '2021-04-24 20:14:10', '2021-04-24 20:14:10', '2021-04-24 21:44:10', 0, 'Abbott', '2f173635170dd6085b14f13c93971283'),
(137, 'Pavithra L', 'drpavithral@gmail.com', 'BTM 2 nd stage Bangalore', 'Sjicr', NULL, NULL, '2021-04-24 20:14:16', '2021-04-24 20:14:16', '2021-04-24 21:44:16', 0, 'Abbott', 'beda359051679f6b2f7a6a35c1ac442b'),
(138, 'Sachit Roy', 'sachit.roy@abbott.com', 'Patna', 'AV', NULL, NULL, '2021-04-24 20:15:41', '2021-04-24 20:15:41', '2021-04-24 21:45:41', 0, 'Abbott', '795458f755bd1ec225ae08eeb62cf81f'),
(139, 'Reshma', 'reshmausman30@yahoo.com', 'MYSURU', 'Columbia asia hospital myzore', NULL, NULL, '2021-04-24 20:15:41', '2021-04-24 20:15:41', '2021-04-24 21:45:41', 0, 'Abbott', '2e7eacee0e5b5aa7f6f923a13a7eb162'),
(140, 'waleem', 'waleempasha@gmail.com', 'Mangaluru ', 'Kmc hospital ', NULL, NULL, '2021-04-24 20:16:16', '2021-04-24 20:16:16', '2021-04-24 21:46:16', 0, 'Abbott', '2c443b5aa94f17fe6ae013cad600f53c'),
(141, 'Jayasheelan M R', 'drjaymr@gmail.com', 'Mysore', 'SJICSR', NULL, NULL, '2021-04-24 20:20:19', '2021-04-24 20:20:19', '2021-04-24 20:32:49', 0, 'Abbott', 'b6d7e765fcf069d0d6e1cf137ac95845'),
(142, 'Prabhakar Koregol', 'drpkore@gmail.com', 'Bangalore ', 'Fortis', NULL, NULL, '2021-04-24 20:28:34', '2021-04-24 20:28:34', '2021-04-24 21:58:34', 0, 'Abbott', 'b213b8c25c3a5038ba596aaf0b42cd62'),
(143, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 20:28:45', '2021-04-24 20:28:45', '2021-04-24 21:58:45', 0, 'Abbott', 'f1e3a4250ab213516076746c46c71af8'),
(144, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-04-24 20:28:47', '2021-04-24 20:28:47', '2021-04-24 21:58:47', 0, 'Abbott', '0defa352201511b188bf0281a8de7ae2'),
(145, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 20:30:17', '2021-04-24 20:30:17', '2021-04-24 22:00:17', 0, 'Abbott', '537e60024ba6670e1a1494c22fd4958c'),
(146, 'Varun Marimuthu', 'varun.marimuthu@gmail.com', 'chennai', 'Sjicsr', NULL, NULL, '2021-04-24 20:34:16', '2021-04-24 20:34:16', '2021-04-24 22:04:16', 0, 'Abbott', '0f8ad7dbfad356f6d48de9a6db973710'),
(147, 'Arun James', 'arun.nelluvelil@abbott.com', 'BANGALORE', 'AV', NULL, NULL, '2021-04-24 20:37:00', '2021-04-24 20:37:00', '2021-04-24 22:07:00', 0, 'Abbott', 'c5081e652dcc3d840daa76d9048bbf2e'),
(148, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 20:38:16', '2021-04-24 20:38:16', '2021-04-24 22:08:16', 0, 'Abbott', 'dcba7d45f9ab37e5a0114b553de5483b'),
(149, 'Thirumaleshwara m', 'theeteonly.m449@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 20:40:49', '2021-04-24 20:40:49', '2021-04-24 22:10:49', 0, 'Abbott', 'fbf8a016e9e8a802bbcbe3d6a47b59ff'),
(150, 'Sudheesh', 'sudheesh.chandran@abbott.com', 'tvm', 'abbott', NULL, NULL, '2021-04-24 20:41:36', '2021-04-24 20:41:36', '2021-04-24 22:11:36', 0, 'Abbott', '06a88bf93fcecf46fa656d6b38adb242'),
(151, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-04-24 20:43:15', '2021-04-24 20:43:15', '2021-04-24 21:35:58', 0, 'Abbott', 'b49f009add37901ad5f518f368ed5c0e'),
(152, 'Thirumaleshwara m', 'theeteonly.m449@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-04-24 20:45:19', '2021-04-24 20:45:19', '2021-04-24 22:15:19', 0, 'Abbott', 'c8251bac8b4fb058479bbbc56634e2bb'),
(153, 'chaudappa ', 'shakapurcj@gmail.com', 'DHARWAD', 'sdmnh', NULL, NULL, '2021-04-24 20:49:16', '2021-04-24 20:49:16', '2021-04-24 22:19:16', 0, 'Abbott', 'cb5e1cac036a66747764a55e0031663d'),
(154, 'Dr Tejeswini CJ', 'docteju27@gmail.com', 'Mysuru', 'JSS', NULL, NULL, '2021-04-24 20:54:54', '2021-04-24 20:54:54', '2021-04-24 22:24:54', 0, 'Abbott', '5e397038105a88331eac5fca33921fa0'),
(155, 'Guru Prasad H P', 'guruprasadhp@yahoo.co.in', 'Mysore', 'Bgs Apollo hospitals Mysore', NULL, NULL, '2021-04-24 20:56:26', '2021-04-24 20:56:26', '2021-04-24 21:35:34', 0, 'Abbott', 'e03d5b2a368d62d5cc2a54caf11c4f34'),
(156, 'chaudappa ', 'shakapurcj@gmail.com', 'DHARWAD', 'sdmnh', NULL, NULL, '2021-04-24 21:06:20', '2021-04-24 21:06:20', '2021-04-24 21:14:48', 0, 'Abbott', '15436eec75bb438c8720260f6cce1085'),
(157, 'Jayasheelan M R', 'drjaymr@gmail.com', 'Mysore', 'SJICSR', NULL, NULL, '2021-04-24 21:11:15', '2021-04-24 21:11:15', '2021-04-24 21:25:59', 0, 'Abbott', 'c73c49a4ff6655458423461b03de2116'),
(158, 'chaudappa ', 'shakapurcj@gmail.com', 'DHARWAD', 'sdmnh', NULL, NULL, '2021-04-24 21:15:20', '2021-04-24 21:15:20', '2021-04-24 22:45:20', 0, 'Abbott', 'ec1d744526ec5077720a0b14a24d0e7a'),
(159, 'Kalyan ', 'kalyan.goswami@abbott.com', 'Bangalore ', 'AV', NULL, NULL, '2021-04-24 21:17:36', '2021-04-24 21:17:36', '2021-04-24 21:36:11', 0, 'Abbott', '3538fc2630e1d6dbbf66fba221520b33'),
(160, 'chaudappa ', 'shakapurcj@gmail.com', 'DHARWAD', 'sdmnh', NULL, NULL, '2021-04-24 21:17:53', '2021-04-24 21:17:53', '2021-04-24 22:47:53', 0, 'Abbott', 'cd9653ef14dfd1a3e6c97ef7558308b1'),
(161, 'Nagamani A C', 'drnagamani_c@yahoo.co.in', 'Bengaluru', 'Jayadeva hospital ', NULL, NULL, '2021-04-24 21:19:22', '2021-04-24 21:19:22', '2021-04-24 21:27:56', 0, 'Abbott', 'b85b147a67d1aaa6075fe6bdaaac0518'),
(162, 'Nagaraja Moorthy', 'drnagaraj_moorthy@yahoo.com', 'Bangalore ', 'Sjicr ', NULL, NULL, '2021-04-24 21:22:31', '2021-04-24 21:22:31', '2021-04-24 22:52:31', 0, 'Abbott', 'd507626f40241f4a54849a05e9e9e8c5'),
(163, 'Drabhisheksharma ', 'drabhisheksharma04@gmail.com', 'Gwalior', 'Apollo', NULL, NULL, '2021-04-24 21:49:11', '2021-04-24 21:49:11', '2021-04-24 23:19:11', 0, 'Abbott', '22debf9536a45d45177017fd459cecda'),
(164, 'Chetana ', 'chetana88@gmail.com', 'Mysore ', 'SJICSR', NULL, NULL, '2021-04-26 16:24:25', '2021-04-26 16:24:25', '2021-04-26 17:54:25', 0, 'Abbott', '5bb8a1745cc7a829a24df5785c7f835b'),
(165, 'Dr Rashmi Rani', 'ranirashmi1974@gmail.com', 'Mysore', 'Chirant Ayurvedic clinic ', NULL, NULL, '2021-04-26 20:36:24', '2021-04-26 20:36:24', '2021-04-26 22:06:24', 0, 'Abbott', '6ad8d5f32cf932ae97e60713b6f23b5b'),
(166, 'Abhinay', 'abhinay.tibdewal@gmail.com', 'Blore', 'Sjicr', NULL, NULL, '2021-04-28 08:53:27', '2021-04-28 08:53:27', '2021-04-28 10:23:27', 0, 'Abbott', '49158f582f00649324b731d97cee1a08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
