<?php
	require_once "../config.php";
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Users</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="nav navbar navbar-light">
    <a class="nav-link" href="#"><img class="navbar-brand logo" src="../img/abbot_logo.png"></a>
</nav>

<div class="container-fluid">
     <div class="row login-info links">   
        <div class="col-8 text-left">
            <a href="questions.php">Questions</a> | <a href="users.php">Users</a>|  <a href="../faculty/admin/faculty.php">Faculty</a> | <a href="https://drive.google.com/file/d/1lkr8vkyouKn2HIDk2BZXRXTzv7afkDU-/view?usp=sharing" target="_blank">Download <span class="badge badge-dark">Download Option Available  only for 7 days</span></a>
            
          
        </div>
        <!-- <div class="text-right">
            <a href="faculty.php" class="">Team details</a>
            </div> -->
    </div>
    <div class="row mt-1">
        <div class="col-12">
            <a href="export_logins.php"><img src="excel.png" height="45" alt=""/></a>
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-12">
            <div id="users"> </div>
        </div>
    </div>
</div>


<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
    getUsers('1');
});

function update(pageNum)
{
  getUsers(pageNum);
}

function getUsers(pageNum)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getusers', page: pageNum},
        type: 'post',
        success: function(response) {
            
            $("#users").html(response);
            
        }
    });
    
}

function logoutUser(uid)
{
   $.ajax({
        url: 'ajax.php',
         data: {action: 'logoutuser', userid: uid},
         type: 'post',
         success: function(output) {
             getUsers('1');
         }
   });
}
</script>

</body>
</html>