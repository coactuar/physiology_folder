<?php
	require_once "../config.php";
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Questions</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="nav navbar navbar-light">
    <a class="nav-link" href="#"><img class="navbar-brand logo" src="../img/abbot_logo.png"></a>
</nav>

<div class="container-fluid">
     <div class="row login-info links">   
        <div class="col-8 text-left">
            <a href="questions.php">Questions</a> | <a href="users.php">Users</a> |  <a href="../faculty/admin/faculty.php">Faculty</a> | <a href="https://drive.google.com/file/d/1lkr8vkyouKn2HIDk2BZXRXTzv7afkDU-/view?usp=sharing" target="_blank">Download <span class="badge badge-dark">Download Option Available  only for 7 days</span></a>
           
        </div>
        <!-- <div class="text-right">
            <a href="faculty.php" class="">Team details</a>
            </div> -->
    </div>    
    <div class="row mt-1">
        <div class="col-12">
            <a href="export_questions.php"><img src="excel.png" height="45" alt=""/></a>
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-12">
            <div id="questions"> </div>
        </div>
    </div>
</div>

<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
getQues('1');
});

function update(pageNum)
{
  getQues(pageNum);
}

function getQues(pageNum)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getquestions', page: pageNum},
        type: 'post',
        success: function(response) {
            
            $("#questions").html(response);
            
        }
    });
    
}

function logout(uid)
{
   $.ajax({
        url: '../ajax.php?id=' + uid,
         data: {action: 'logoutuser'},
         type: 'post',
         success: function(output) {
             $("#logins").load("../ajax.php?action=getlogins&page=1");
         }
   });
}

function getupdate()
{
	var curr = $('#ques_count').text();
	//alert(curr);
	
	$.ajax({ url: 'ajax.php',
         data: {action: 'getquesupdate'},
         type: 'post',
         success: function(output) {
					 var msg = output;
                     
					 if (Number(output) > Number(curr))
					 {
						 newmsg = Number(output) - Number(curr);
						 if(newmsg == "1")
						 { 
						 msg = newmsg+ " new question available.";
						 }
						 else{
						 msg = newmsg+ " new questions available.";
						 }
						 msg += " <a href='questions.php'>Refresh</a>";
					 }
					 else
					 {
						 msg="";
					 }
					 
					 	$("#ques_update").html(msg);
					 
                  }
});
}

setInterval(function(){ getupdate(); }, 10000);

function updateQues(id)
{
    var qid = '#answer'+id;
    var curval = $(qid).val(); 
    $.ajax({
        url: 'ajax.php?id=' + id,
         data: {action: 'updateques', val: curval },
         type: 'post',
         success: function(output) {
             //alert(output);
             getQues('1');
         }
   });   
}

function updSpk(qid, spk)
{
    $.ajax({
        url: 'ajax.php?',
         data: {action: 'updatespk', ques:qid, val: spk },
         type: 'post',
         success: function(output) {
             getQues('1');
         }
   });   
    
}
function updSpkAns(qid, ans)
{
    $.ajax({
        url: 'ajax.php?',
         data: {action: 'updatespkans', ques:qid, val: ans },
         type: 'post',
         success: function(output) {
             getQues('1');
         }
   });   
    
}


</script>

</body>
</html>