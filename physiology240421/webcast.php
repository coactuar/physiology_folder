<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_token"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_datexx   = date('Y/m/d H:i:s');
          
			
            $tokenxs = $_SESSION['user_token'];
            $query="UPDATE tbl_users set logout_date='$logout_datexx', logout_status='0' where token='$tokenxs' ";
             $res = mysqli_query($link, $query) or die(mysqli_error($link));
			//print_r($query);
            unset($_SESSION["user_name"]);
            unset($_SESSION["user_email"]);
            unset($_SESSION["user_city"]);
            unset($_SESSION["user_hospital"]);
            unset( $_SESSION['user_token']);
			
            header("location: ./");
            exit;
        }
		if($action == "autologout")
        {
            $logout_datexx   = date('Y/m/d H:i:s');
          
			
            $tokenxs = $_SESSION['user_token'];
			
			$name = $_SESSION["user_name"];
			$email = $_SESSION["user_email"];
			$city = $_SESSION["user_city"];
			$hospital = $_SESSION["user_hospital"];
			
            $query="UPDATE tbl_users set logout_date='$logout_datexx', logout_status='0' where token='$tokenxs' ";
             $res = mysqli_query($link, $query) or die(mysqli_error($link));
			//print_r($query);
            unset($_SESSION["user_name"]);
            unset($_SESSION["user_email"]);
            unset($_SESSION["user_city"]);
            unset($_SESSION["user_hospital"]);
            unset( $_SESSION['user_token']);
			
            header("location: ./index.php?name=$name&city=$city&email=$email&hospital=$hospital");
            exit;
        }

    }
	

    
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Abbott</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">

<script>
//.goal('9habuht6gh0ajh4q');
</script>

<!-- Pixel Code for https://datastudio.co.in/ -->
<script async src="https://datastudio.co.in/pixel/Tg1gpOPVf50UH7ja" data-custom-parameters='{"name": "<?= $_SESSION['user_name'];?>", "email": "<?= $_SESSION['user_email'];?>"}'></script>
<!-- END Pixel Code -->



</head>

<body>
<div class="container-fluid">
    <div class="row logo-nav">
        <div class="col-12 col-md-4">
              <img src="img/abbot_logo.png" class="img-fluid logo" style="max-height:90px;" alt=""/>  
        </div>
        <div class="col-12 col-md-4 offset-md-4 text-right">
           <!-- <img src="img/apdrops.jpg" class="img-fluid logo" alt=""/> -->
        </div>
    </div>
    <div class="row login-info ">
        <div class="col-12 p-1 text-right">
          Hello, <?php echo $_SESSION['user_name']; ?>! <a class="btn btn-sm btn-light" href="?action=logout">Logout</a>
        </div>
    </div>
    <div class="row mt-4">
      <div class="col-12 col-md-7">
            <div class="embed-responsive embed-responsive-16by9">
         
            <iframe src="https://vimeo.com/event/916627/embed" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe>
                  </div>
              </div>
        <div class="col-12 col-md-5">
          <div id="question" class="mb-3">
              <div id="question-form" class="panel panel-default">
                  <form method="POST" action="#" class="form panel-body" role="form">
                      <div class="row">
                          <div class="col-12">
                       
                          <div id="ques-message"></div>
                          <div class="form-group">
                              <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="5"></textarea>
                          </div>
                          
                          </div>
                          <div class="col-12">
                          <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_name']; ?>">
                          <input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['user_email']; ?>">
                          <button class="btn btn-primary btn-sm btn-submit" type="submit">Submit your Question</button>
                          </div>
                      </div>
                </form>
              </div>
          </div>
		  <!--
          <center><br>
        <img src="img/logo2.jpg" class="img-fluid" alt=""/> 
        </center>
		-->
        </div>
    </div>
	
	<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
       
        <button type="button" data-login="0" class="btn btn-primary" onclick="clicklogin()">Login</button>
      </div>
    </div>
  </div>
</div>
    
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){
    //$('#videolinks').html('If you are unable to watch video, <a href="#" onClick="changeVideo()">click here</a>');
	$(document).on('submit', '#question-form form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#ques-message').text('Your question is submitted successfully.');
                  $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#ques-message').text(data);
                  $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });
});
/*
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			 console.log(output);
         }
});
}
setInterval(function(){ update(); }, 10000);

function alive()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'alive'},
         type: 'post',
         success: function(output) {
			 console.log(output);
			 if(output == 's')
				{
					window.location.href='?action=logout';  
				}
         }
});
}
setInterval(function(){ alive(); }, 5000);

*/
</script>


<script>
/*
let log;
let ti = 0;
  function handleVisibilityChange() {
    // window.onbeforeunload = function() {
    // return true;
    // }
  if (document.hidden) {
	 
     //alert('Hidden')
	// setInterval(function(){ window.location.href="?action=logout"; }, 900000);
      //window.location.href="?action=logout";
	  // ti = 0;
	  // $('#exampleModalCenter').modal('show'); 
	 // console.log(ti);
	//  autologout(ti);
	
	 window.location.href="?action=autologout";
    
  } else  {
	 
    //alert('Vissible')
  }

 
}
document.addEventListener("visibilitychange", handleVisibilityChange, true);
*/
/*
function clicklogin(){
	ti = 1;
	 $('#exampleModalCenter').modal('hide'); 
	  
	   autologout(ti);
}
function autologout(ti){
	console.log('working'.ti);
	
if(ti != 0){
 console.log('do nothing ');

}else{
	console.log('close');
	setInterval(function(){ 
	
	  $('#exampleModalCenter').modal('hide'); 
	  window.location.href="?action=logout";
	
	},5000);
}

}
 */
</script>
</body>
</html>