
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Abbott</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="container-fluid">
    <div class="row logo-nav">
        <div class="col-12 col-md-4">
            <img src="img/abbot_logo.png" class="img-fluid logo" style="max-height:90px;" alt=""/> 
        </div>
        <div class="col-12 col-md-4 offset-md-4 text-right">
           <!-- <img src="img/apdrops.jpg" class="img-fluid logo" alt=""/> -->
        </div>
    </div>
    <div class="row mt-2">
      <div class="col-12 col-md-6 col-lg-5 offset-lg-1 text-center">
 	  
         <img src="../img/Physiology Masterclass - Webinar without view lecture-01.jpg" class="img-fluid"  alt=""/>  
      </div>
      <div class="col-12 col-md-6 col-lg-4">
            <div class="login">
                <form id="login-form" method="post" role="form">
                  <div id="login-message"></div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="basic-addon1" name="name" id="name" required>
                  </div>              
                  <div class="input-group">
                    <input type="email" class="form-control" placeholder="Email" aria-label="Email" aria-describedby="basic-addon1" name="email" id="email" required>
                  </div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="City" aria-label="City" aria-describedby="basic-addon1" name="city" id="city"  required >
                  </div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Hospital" aria-label="Hospital" aria-describedby="basic-addon1" name="hospital" id="hospital" required>
                  </div>
                  <div class="input-group">
                    <button id="login" class="btn btn-primary btn-sm login-button" type="submit">Login</button>
                  </div>
                </form>
            </div>
        
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
  });
  
  return false;
});
</script>


</body>
</html>